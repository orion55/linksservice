# Сервис сокращения ссылок
Простой сервис для сокращения ссылок 
VueJS\Pinia+Vite \ C# + Asp.Net Core + EntityFramework + PostgreSQL \ GitLab + Docker + Docker-composer<br/>
## Демо
Url: http://62-113-100-41.nip.io/ <br/>

## Развёртывание и компиляция проекта
Вот краткое введение о том, что должен сделать разработчик, чтобы начать дальнейшую разработку проекта:
```
git clone https://gitlab.com/orion55/linksservice.git
cd .\client\
npm install
npm start build:dto
cd ..\server\LinksService.API\
dotnet build "LinksService.API.csproj"
cd ..\..\client\
npm run dev
```
## Ссылки

- Домашняя страница: http://62-113-100-41.nip.io/
- Repository: https://gitlab.com/orion55/linksservice/-/tree/main


## Licensing

Код в этом проекте распространяется по лицензии MIT.
