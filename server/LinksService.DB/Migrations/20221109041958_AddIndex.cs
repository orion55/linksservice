﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LinksService.DB.Migrations
{
    public partial class AddIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_links_code",
                table: "links",
                column: "code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_links_code",
                table: "links");
        }
    }
}
