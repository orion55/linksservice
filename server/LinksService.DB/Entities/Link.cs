﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LinksService.DB.Entities
{
    [Table("links")]
    public class Link
    {
        [Column("id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("account_id")]
        [ForeignKey(nameof(Account))]
        public int? AccountId { get; set; }

        public virtual Account? Account { get; set; }

        [Column("url")]
        public string? Url{ get; set; }

        [Column("code")]
        public string? Code { get; set; }

        [Column("created_utc")]
        public DateTime CreatedUtc { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; } = true;
    }
}
