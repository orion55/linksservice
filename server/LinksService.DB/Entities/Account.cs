﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LinksService.DB.Entities
{
    [Table("accounts")]
    public class Account
    {
        [Column("id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("email")]
        public string? Email { get; set; }

        [Column("password_hash")]        
        public byte[]? PasswordHash { get; set; }
        
        [Column("password_salt")]        
        public byte[]? PasswordSalt { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; } = true;
    }
}
