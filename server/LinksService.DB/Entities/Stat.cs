﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LinksService.DB.Entities
{
    [Table("stats")]
    public class Stat
    {
        [Column("id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("link_id")]
        [ForeignKey(nameof(Link))]
        public int LinkId { get; set; }
        public virtual Link? Link { get; set; }

        [Column("click_utc")]
        public DateTime ClickUtc { get; set; }

        [Column("ip")]
        public string? IP { get; set; }

        [Column("user_agent")]
        public string? UserAgent { get; set; }

        [Column("refer")]
        public string? Refer { get; set; }
    }
}
