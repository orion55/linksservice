﻿using LinksService.DB.Entities;
using Microsoft.EntityFrameworkCore;

namespace LinksService.DB.Infrastructure
{
    public class LinksDbContext : DbContext
    {
        public LinksDbContext() { }

        public LinksDbContext(DbContextOptions<LinksDbContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Account>? Accounts { get; set; }
        public DbSet<Link>? Links { get; set; }
        public DbSet<Stat>? Stats { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (optionsBuilder.IsConfigured) return;

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ??
                                  Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseNpgsql(connectionString);
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Stat>().HasIndex(s => s.ClickUtc);
            modelBuilder.Entity<Link>().HasIndex(s => s.Code);
        }
    }
}
