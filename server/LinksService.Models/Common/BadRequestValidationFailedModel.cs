﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FluentValidation.Results;

namespace LinksService.Models.Common
{
    public class BadRequestValidationFailedModel : BadRequestModel
    {
        public BadRequestValidationFailedModel()
        {
            ErrorMessage = "Validation failed";
        }

        public IEnumerable<ValidationFailure> ValidationFailures { get; set; }
    }
}