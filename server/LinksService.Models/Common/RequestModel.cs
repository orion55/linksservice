﻿using System.Runtime.Serialization;

namespace LinksService.Models.Common
{
    /// <summary>
    /// Generic request model used in Web API
    /// </summary>
    /// <typeparam name="TParams"></typeparam>
    public class RequestModel<TParams>
    {
        public TParams Params { get; set; }
    }
}
