﻿namespace LinksService.Models.Common;

public class ForbiddenResultModel : BadRequestModel
{
	public ForbiddenResultModel() { }

	public ForbiddenResultModel(string errorMessage) 
		: base(errorMessage)
	{ }

	public ForbiddenResultModel(string[] errorMessages) 
		: base(errorMessages)
	{ }

	public ForbiddenResultModel(string title, string errorMessage)
		: base(title, errorMessage)
	{ }
}
