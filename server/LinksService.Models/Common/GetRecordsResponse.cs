﻿using System.Collections.Generic;

namespace LinksService.Models.Common
{
    public class GetRecordsResponse<TRecord>
    {
        public int TotalRecords { get; set; }
        public int TotalFilteredRecords { get; set; }
        public IEnumerable<TRecord> Records { get; set; }

        public GetRecordsResponse() { }

        public GetRecordsResponse(int totalRecords, int totalFilteredRecords, IEnumerable<TRecord> records)
        {
            TotalRecords = totalRecords;
            TotalFilteredRecords = totalFilteredRecords;
            Records = records;
        }
    }
}