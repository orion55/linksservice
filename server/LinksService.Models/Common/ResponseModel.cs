﻿using System.Runtime.Serialization;

namespace LinksService.Models.Common
{
    /// <summary>
    /// Generic response model used in Web API
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class ResponseModel<TData>
    {
        public ResponseModel(TData data)
        {
            Data = data;
        }

        public TData Data { get; set; }
    }
}