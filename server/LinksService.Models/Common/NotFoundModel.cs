﻿using System.Runtime.Serialization;

namespace LinksService.Models.Common
{
    public class NotFoundModel : BadRequestModel
    {
        public int Id { get; set; }

        public NotFoundModel() { }

        public NotFoundModel(int id, string message)
            : base(message)
        {
            Id = id;
        }
    }
}
