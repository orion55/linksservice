﻿using System.Runtime.Serialization;

namespace LinksService.Models.Common
{
    public class BadRequestModel
    {
        public string ErrorMessage { get; set; }
        
        public string Title { get; set; }

        public string[] ErrorMessages { get; set; }

        public BadRequestModel() { }

        public BadRequestModel(string errorMessage) 
        {
            ErrorMessage = errorMessage;
        }

        public BadRequestModel(string[] errorMessages)
        {
            ErrorMessages = errorMessages;
        }

        public BadRequestModel(string title, string errorMessage)
        {
            Title = title;
            ErrorMessage = errorMessage;
        }
    }
}