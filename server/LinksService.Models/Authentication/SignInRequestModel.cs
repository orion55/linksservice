﻿using System.Runtime.Serialization;

namespace LinksService.Models.Authentication
{
    /// <summary>
    /// Model to sign in user
    /// </summary>    
    #pragma warning disable CS8618
    public class SignInRequestModel
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }       
    }
}