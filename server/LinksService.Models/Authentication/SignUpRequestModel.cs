﻿using System.Runtime.Serialization;

namespace LinksService.Models.Authentication
{
#pragma warning disable CS8618
    public class SignUpRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
    }
}
