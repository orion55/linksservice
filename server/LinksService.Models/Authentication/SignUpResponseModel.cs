﻿using System;
using System.Runtime.Serialization;

namespace LinksService.Models.Authentication
{
    #pragma warning disable CS8618
    public class SignUpResponseModel
    {
        public string Token { get; set; }
    }
}