﻿using System.Runtime.Serialization;

namespace LinksService.Models.Authentication
{
    /// <summary>
    /// Model returned in case of successful sign in
    /// </summary>
    public class SignInResponseModel
    {
        /// <summary>
        /// JWT token
        /// </summary>
        public string Token { get; set; }
    }
}