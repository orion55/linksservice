﻿namespace LinksService.Models.Redirect
{
    public class RedirectModel
    {
        public string? Url { get; set; }
    }
}
