﻿namespace LinksService.Models.Links
{
    public class StatModel
    {
        public int Id { get; set; }
        public DateTime ClickUtc { get; set; }
        public string? IP { get; set; }
        public string? UserAgent { get; set; }
        public string? Refer { get; set; }
    }
}
