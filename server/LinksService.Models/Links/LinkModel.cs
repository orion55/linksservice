﻿namespace LinksService.Models.Links
{
    public class LinkModel
    {
        public int Id { get; set; }
        public int? AccountId { get; set; }

        public string? Url { get; set; }

        public string? Code { get; set; }

        public DateTime CreatedUtc { get; set; }

        public bool IsActive { get; set; }
    }
}
