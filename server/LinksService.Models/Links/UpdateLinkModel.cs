﻿namespace LinksService.Models.Links
{
    public class UpdateLinkModel
    {
        public string? Url { get; set; }
        public bool IsActive { get; set; }
    }
}
