﻿using System.Runtime.Serialization;

namespace LinksService.Models.Links
{
    /// <summary>
    /// The model is returned if the link is successfully created  
    /// </summary>
    #pragma warning disable CS8618
    public class CreateLinksResponseModel
    {
        /// <summary>
        /// Short Code Link
        /// </summary>
        public string Code { get; set; }
    }
}
