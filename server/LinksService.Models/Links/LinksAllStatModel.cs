﻿namespace LinksService.Models.Links
{
    public class StatLinksAllModel
    {
        public int CountLinks { get; set; }
        public int TotalClicks { get; set; }
        public int MonthClicks { get; set; }
        public int DayClicks { get; set; }
        public string[]? UniqIP { get; set; }
        public string[]? UniqUserAgent { get; set; }
        public string[]? UniqRefer { get; set; }
    }
}
