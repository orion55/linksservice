﻿using System.Runtime.Serialization;

namespace LinksService.Models.Links
{
    /// <summary>
    /// Model to create link    
    /// </summary>
    #pragma warning disable CS8618
    public class CreateLinksRequestModel
    {
        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }
    }
}
