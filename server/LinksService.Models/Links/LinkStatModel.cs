﻿namespace LinksService.Models.Links
{
    public class LinkStatModel
    {
        public int Id { get; set; }
        public int TotalClicks { get; set; }
        public int MonthClicks { get; set; }
        public int DayClicks { get; set; }
        public IEnumerable<StatModel> Records { get; set; }
    }
}
