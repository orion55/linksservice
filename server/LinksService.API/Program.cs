using LinksService.Core.Infrastructure.Extensions;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR;
using LinksService.Core.Infrastructure.Middlewares;
using LinksService.Core.Infrastructure.Validators.Accounts;
using LinksService.DB.Infrastructure;
using LinksServices.Api;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using System.Text;
using FluentValidation;
using LinksService.Core.Infrastructure.MediatR.Behaviors;
using LinksService.Core.AutoMapperProfiles;

try
{
    var builder = WebApplication.CreateBuilder(args);
    SerilogExtension.AddSerilogApi(builder.Configuration);
    builder.Host.UseSerilog(Log.Logger);

    var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ??
                              Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");

    builder.Configuration.SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile("appsettings.json", true)
            .AddJsonFile($"appsettings.{environmentName}.json", true)
            .AddEnvironmentVariables();

    string connection = builder.Configuration.GetConnectionString("DefaultConnection");

    builder.Services.AddDbContext<LinksDbContext>(options =>
    {
        options.UseNpgsql(
            connection,
            builder => builder.EnableRetryOnFailure(10, TimeSpan.FromSeconds(5), null)
        );
    }, ServiceLifetime.Scoped);

    builder.Services
        .AddMediatR(typeof(DummyCommand))
        .AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>))
        .AddValidatorsFromAssemblyContaining(typeof(SignInCommandValidator))
        .AddAutoMapper(typeof(LinksProfile)); ;

    builder.Services.AddHttpContextAccessor();
    builder.Services.AddControllers();

    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "LinkServiceApi", Version = "v1" });
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Scheme = "bearer",
            Description = "Please insert JWT token into field"
        });

        c.OperationFilter<AuthorizationOperationFilter>();
    });

    builder.Services
        .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = builder.Configuration["JwtOptions:Issuer"],
                ValidateAudience = true,
                ValidAudience = builder.Configuration["JwtOptions:Audience"],
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JwtOptions:Key"]))
            };
        });
    builder.Services.AddScoped<AuthHelper>();
    
    builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
    {
        builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
    }));

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseMiddleware<ErrorHandlingMiddleware>();
    app.UseMiddleware<ValidationMiddleware>();
    app.UseMiddleware<RequestSerilLogMiddleware>();

    app.UseCors("corsapp");
    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
    Log.Information("Server Shutting down...");
    Log.CloseAndFlush();
}
