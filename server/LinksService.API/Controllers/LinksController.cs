﻿using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Entities;
using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.Design;

namespace LinksService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinksController : LinksBaseController
    {
        public LinksController(IMediator mediator) : base(mediator)
        { }

        [HttpPost]
        [AllowAnonymous]
        [Route(nameof(Create))]
        [ProducesResponseType(typeof(ResponseModel<CreateLinksResponseModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestValidationFailedModel>), StatusCodes.Status400BadRequest)]        
        public async Task<IActionResult> Create(RequestModel<CreateLinksRequestModel> request,
    CancellationToken cancellationToken)
        {
            return await SendCommand(new CreateLinksCommand(request.Params), cancellationToken);
        }

        [HttpGet]
        [Authorize]        
        [Route("{linkId}")]
        [ProducesResponseType(typeof(ResponseModel<LinkModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestValidationFailedModel>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetLinkById([FromRoute] int linkId,
            CancellationToken cancellationToken)
        {
            return await SendCommand(new GetLinkCommand(linkId), cancellationToken);
        }

        [HttpGet]
        [Authorize]
        [Route("")]
        [ProducesResponseType(typeof(ResponseModel<ICollection<LinkModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetLinksAll(CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new GetLinksAllCommand(), cancellationToken);
            return Ok(new ResponseModel<ICollection<LinkModel>>(result));
        }

        [HttpPut]
        [Authorize]
        [Route("{linkId}")]
        [ProducesResponseType(typeof(ResponseModel<LinkModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestValidationFailedModel>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateLink([FromRoute] int linkId, RequestModel<UpdateLinkModel> request,
           CancellationToken cancellationToken)
        {
            return await SendCommand(new UpdateLinkCommand(linkId, request.Params), cancellationToken);
        }

        [HttpDelete]
        [Authorize]
        [Route("{linkId}")]
        [ProducesResponseType(typeof(ResponseModel<LinkModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestValidationFailedModel>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteLink([FromRoute] int linkId, CancellationToken cancellationToken)
        {
            var command = new DeleteLinkCommand(linkId);
            return await SendCommand(command, cancellationToken);            
        }

        [HttpGet]
        [Authorize]
        [Route("{linkId}/stat")]
        [ProducesResponseType(typeof(ResponseModel<LinkStatModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]        
        public async Task<IActionResult> GetStatLinkById([FromRoute] int linkId,
            CancellationToken cancellationToken)
        {
            return await SendCommand(new GetLinkStatCommand(linkId), cancellationToken);
        }

        [HttpGet]
        [Authorize]
        [Route("stat")]
        [ProducesResponseType(typeof(ResponseModel<StatLinksAllModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetStatLinksAll(CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new GetStatLinksAllCommand(), cancellationToken);
            return Ok(new ResponseModel<StatLinksAllModel>(result));
        }
    }
}
