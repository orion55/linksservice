﻿using LinksService.Core.Infrastructure.MediatR.Commands.RedirectCommands;
using LinksService.Models.Common;
using LinksService.Models.Redirect;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LinksService.API.Controllers
{
    [Route("t/")]
    [ApiController]
    public class RedirectController : LinksBaseController
    {
        public RedirectController(IMediator mediator) : base(mediator)
        { }

        [HttpGet]
        [AllowAnonymous]
        [Route("{code}")]
        [ProducesResponseType(typeof(ResponseModel<RedirectModel>), StatusCodes.Status301MovedPermanently)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]        
        public async Task<IActionResult> RedirectionByCode([FromRoute] string code, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new RedirectCommand(code), cancellationToken);

            return result.Match(                
                model => RedirectPermanent(model.Url) as IActionResult,
                error => BadRequest(new ResponseModel<BadRequestModel>(error))
            );
        }
    }
}
