﻿using LinksService.Models.Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using OneOf;

namespace LinksService.API.Controllers
{
    [ApiController]
    public class LinksBaseController : ControllerBase
    {
        protected readonly IMediator _mediator;

        public LinksBaseController(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected virtual ObjectResult ForbiddenResult([ActionResultObjectValue] ForbiddenResultModel error)
        {
            var responseModel = new ResponseModel<ForbiddenResultModel>(error);
            return new ObjectResult(responseModel) { StatusCode = StatusCodes.Status403Forbidden };
        }

        protected async Task<IActionResult> SendCommand<TResponse>(IRequest<OneOf<TResponse, ForbiddenResultModel>> command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);

            return result.Match(
                model => Ok(new ResponseModel<TResponse>(model)) as IActionResult,
                error => ForbiddenResult(error));
        }

        protected async Task<IActionResult> SendCommand<TResponse>(IRequest<OneOf<TResponse, BadRequestModel>> command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);

            return result.Match(
                model => Ok(new ResponseModel<TResponse>(model)) as IActionResult,
                error => BadRequest(new ResponseModel<BadRequestModel>(error)));
        }

        protected async Task<IActionResult> SendCommand<TResponse>(
            IRequest<OneOf<TResponse, BadRequestModel, ForbiddenResultModel>> command,
            CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);

            return result.Match(
                model => Ok(new ResponseModel<TResponse>(model)) as IActionResult,
                error => BadRequest(new ResponseModel<BadRequestModel>(error)),
                ForbiddenResult);
        }

        protected async Task<IActionResult> SendCommand<TResponse>(
            IRequest<OneOf<TResponse, BadRequestModel, NotFoundModel>> command,
            CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);

            return result.Match(
                model => Ok(new ResponseModel<TResponse>(model)) as IActionResult,
                error => BadRequest(new ResponseModel<BadRequestModel>(error)),
                notFound => BadRequest(new ResponseModel<NotFoundModel>(notFound)));
        }
    }
}
