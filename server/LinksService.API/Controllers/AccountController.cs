﻿using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.Models.Authentication;
using LinksService.Models.Common;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LinksService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : LinksBaseController
    {
        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger, IMediator mediator) : base(mediator)
        {
            _logger = logger;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route(nameof(SignUp))]
        [ProducesResponseType(typeof(ResponseModel<SignUpResponseModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestValidationFailedModel>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignUp(RequestModel<SignUpRequestModel> request,
            CancellationToken cancellationToken)
        {
            return await SendCommand(new SignUpCommand(request.Params), cancellationToken);           
        }

        [HttpPost]
        [AllowAnonymous]
        [Route(nameof(SignIn))]
        [ProducesResponseType(typeof(ResponseModel<SignInResponseModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseModel<BadRequestModel>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignIn(RequestModel<SignInRequestModel> request, CancellationToken cancellationToken)
        {            
            return await SendCommand(new SignInCommand(request.Params), cancellationToken);
        }

        [HttpPost]
        [Authorize]
        [Route(nameof(SignOut))]
        [ProducesResponseType(typeof(ResponseModel<DummyResponseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> SignOut(RequestModel<DummyRequestModel> request,
    CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(new SignOutCommand(), cancellationToken);
            return result.Match(model => Ok(new ResponseModel<DummyResponseModel>(new DummyResponseModel())));
        }
    }
}
