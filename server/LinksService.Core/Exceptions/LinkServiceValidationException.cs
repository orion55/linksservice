﻿using FluentValidation.Results;

namespace LinksService.Core.Exceptions
{
    public class LinkServiceValidationException : Exception
    {
        public IEnumerable<ValidationFailure> ValidationFailures { get; set; }
    }
}
