﻿namespace LinksService.Core.Constants
{
    public class ErrorMessages
    {
        #region CommonErrors

        public const string ModelShouldNotBeNull = "Model should not be null";
        public const string QueryParamsShouldNotBeNull = "Query params should not be null";
        public const string TakeShouldBeGreaterOrEqualThanZero = "Take should be greater or equal than zero";
        public const string SkipShouldBeGreaterOrEqualThanZero = "Skip should be greater or equal than zero";
        public const string FilterShouldNotBeNull = "Filter should not be null";
        public const string SortShouldNotBeNull = "Sort should not be null";
        public const string NotFoundInDb = "Entity not found";

        #endregion

        public class AccountMessages
        {
            #region EmailErrors

            public const string EmailAlreadyExists = "This email is already registered";
            public const string EmailShouldNotBeNull = "Email should not be null";
            public const string EmailShouldNotBeEmpty = "Email should not be empty";
            public const string EmailIsNotValid = "\"Email\" is not a valid email address";
            public const string EmailDoesNotExist = "Account for the provided email does not exist";

            #endregion

            #region PasswordErrors
            public const string PasswordShouldNotBeNull = "Password should not be null";
            public const string PasswordShouldNotBeEmpty = "Password should not be empty";
            public const string PasswordCannotBeLessThan8 = "Password cannot be less than 8 characters";
            public const string PasswordMustContainSpecialSymbol = "Password must contain at least one special symbol";
            public const string PasswordMustContainNumber = "Password must contain at least one number";
            public const string PasswordAreNotTheSame = "Passwords aren't the same";
            #endregion 
        }

        public class LinksErrors
        {
            public const string LinkAlreadyExists = "The link exists";
            public const string UrlShouldNotBeNull = "Url should not be null";
            public const string UrlShouldNotBeEmpty = "Url should not be empty";
            public const string IsActiveShouldNotBeEmpty = "isActive should not be empty";
            public const string LinkAndUserNotFound = "The link was not found with the current user";
            public const string LinkNotFound = "The link was not found";
            public const string LinkNotActive = "The link was not active";
        }
        public class RedirectErrors
        {
            public const string CodeShouldNotBeNull = "Code should not be null";
            public const string CodeShouldNotBeEmpty = "Code should not be empty";
        }
    }
}
