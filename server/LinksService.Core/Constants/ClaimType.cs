﻿namespace LinksService.Core.Constants
{
    public class ClaimType
    {
        public const string UserId = "user_id";
        public const string Email = "email";
    }
}
