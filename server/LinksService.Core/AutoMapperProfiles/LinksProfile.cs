﻿using AutoMapper;
using LinksService.DB.Entities;
using LinksService.Models.Links;

namespace LinksService.Core.AutoMapperProfiles
{
    public class LinksProfile : Profile
    {
        public LinksProfile()
        {
            CreateMap<LinkModel, Link>().ReverseMap();           
        }
    }
}
