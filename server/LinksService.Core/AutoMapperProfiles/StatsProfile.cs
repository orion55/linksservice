﻿using AutoMapper;
using LinksService.DB.Entities;
using LinksService.Models.Links;

namespace LinksService.Core.AutoMapperProfiles
{
    public class StatsProfile : Profile
    {
        public StatsProfile() {
            CreateMap<StatModel, Stat>().ReverseMap();
        }
    }
}
