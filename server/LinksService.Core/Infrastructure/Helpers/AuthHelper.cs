﻿using LinksService.Core.Constants;
using LinksService.DB.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LinksService.Core.Infrastructure.Helpers
{
    public class AuthHelper
    {       
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthHelper(IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;

        }
        public string CreateToken(Account user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimType.UserId, $"{user.Id:D}"),
                new Claim(ClaimType.Email, $"{user.Email}"),
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
            _configuration.GetSection("JwtOptions:Key").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            double minutes = Convert.ToDouble(_configuration.GetSection("JwtOptions:ExpiryDuration").Value);
            DateTime expires = DateTime.Now.AddMinutes(minutes);

            var token = new JwtSecurityToken(
                issuer: _configuration.GetSection("JwtOptions:Issuer").Value,
                audience: _configuration.GetSection("JwtOptions:Audience").Value,
                claims: claims,
                expires: expires,
                signingCredentials: creds);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public int? GetUserId() 
        {
            var authorization = _httpContextAccessor.HttpContext?.Request?.Headers["Authorization"].ToString();
            int? userId = null;

            if (!string.IsNullOrEmpty(authorization))
            {
                var token = authorization.Split(" ")[1];
                var handler = new JwtSecurityTokenHandler();
                var jwtSecurityToken = handler.ReadJwtToken(token);
                userId = int.Parse(jwtSecurityToken.Claims.First(claim => claim.Type == ClaimType.UserId).Value);
            }

            return userId;
        }
    }
}
