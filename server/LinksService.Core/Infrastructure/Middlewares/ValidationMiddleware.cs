﻿using System.Net.Mime;
using System.Threading.Tasks;
using LinksService.Core.Exceptions;
using LinksService.Models.Common;
using Microsoft.AspNetCore.Http;

namespace LinksService.Core.Infrastructure.Middlewares
{
    public class ValidationMiddleware
    {
        private readonly RequestDelegate _next;

        public ValidationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next.Invoke(httpContext);
            }
            catch (LinkServiceValidationException exception)
            {
                await WriteBadRequestStatusResponseAsync(httpContext, exception);
            }
        }

        private static async Task WriteBadRequestStatusResponseAsync(HttpContext httpContext,
            LinkServiceValidationException exception)
        {
            httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            httpContext.Response.ContentType = MediaTypeNames.Application.Json;

            await httpContext.Response.WriteAsJsonAsync(new ResponseModel<BadRequestValidationFailedModel>(new BadRequestValidationFailedModel
            {
                ValidationFailures = exception.ValidationFailures
            }));
        }
    }
}