﻿using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Filters;

namespace LinksService.Core.Infrastructure.Extensions;

public static class SerilogExtension
{
    public static void AddSerilogApi(IConfiguration configuration)
    {
        //var template = "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Properties:j}{NewLine}{Exception}";
        var template = "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}";

        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails()
            .Enrich.WithCorrelationId()
            .Enrich.WithProperty("ApplicationName", $"API Serilog - {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}")
            .Filter.ByExcluding(Matching.FromSource("Microsoft.AspNetCore.StaticFiles"))            
            .WriteTo.Async(wt => wt.Console(outputTemplate: template))
            .WriteTo.Async(a => a.File("./logs/logs-.log", rollingInterval: RollingInterval.Day, outputTemplate: template))
            .CreateLogger();
    }
}
