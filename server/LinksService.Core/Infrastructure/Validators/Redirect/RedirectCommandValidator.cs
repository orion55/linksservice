﻿using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.RedirectCommands;

namespace LinksService.Core.Infrastructure.Validators.Redirect
{
    public class RedirectCommandValidator : BaseValidator<RedirectCommand, string>
    {
        public RedirectCommandValidator()
        {
            ConfigureValidation();
        }

        protected override void ConfigureValidation()
        {
            base.ConfigureValidation();
            RuleFor(x => x.Model)                
                .NotEmpty(ErrorMessages.RedirectErrors.CodeShouldNotBeEmpty);
        }
    }
}
