﻿using FluentValidation;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.Validators;
using LinksService.DB.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace LinksService.Core.Infrastructure.Validators
{
    public static class StandartValidators
    {
        public static IRuleBuilderOptions<T, P> NotNull<T, P>(this IRuleBuilder<T, P> rule, string errorMessage) where P : class
        {
            return rule
                    .NotNull()
                    .WithMessage(errorMessage);
        }

        public static IRuleBuilderOptions<T, P?> NotNull<T, P>(this IRuleBuilder<T, P?> rule, string errorMessage) where P : struct
        {
            return rule
                    .NotNull()
                    .WithMessage(errorMessage);
        }

        public static IRuleBuilderOptions<T, string> NotEmpty<T>(this IRuleBuilder<T, string> rule, string errorMessage)
        {
            return rule
                    .NotEmpty()
                    .WithMessage(errorMessage);
        }

        public static IRuleBuilderOptions<T, string> EMail<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotNull(ErrorMessages.AccountMessages.EmailShouldNotBeNull)
                .NotEmpty(ErrorMessages.AccountMessages.EmailShouldNotBeEmpty)
                .EmailAddress()
                .WithMessage(ErrorMessages.AccountMessages.EmailIsNotValid);
        }

        public static IRuleBuilderOptions<T, string> EmailExists<T>(this IRuleBuilder<T, string> rule, IServiceScopeFactory serviceScopeFactory)
        {
            return rule
                .MustAsync(async (email, cancellationToken) =>
                {
                    using var scopeFactory = serviceScopeFactory.CreateScope();
                    var db = scopeFactory.ServiceProvider.GetRequiredService<LinksDbContext>();
                    return await db.Accounts.AnyAsync(x => x.Email.ToLower() == email.ToLower(), cancellationToken);
                })
                .WithMessage(ErrorMessages.AccountMessages.EmailDoesNotExist);
        }

        public static IRuleBuilderOptions<T, string> EmailNotExists<T>(this IRuleBuilder<T, string> rule, IServiceScopeFactory serviceScopeFactory)
        {
            return rule
                .MustAsync(async (email, cancellationToken) =>
                {
                    using var scopeFactory = serviceScopeFactory.CreateScope();
                    var db = scopeFactory.ServiceProvider.GetRequiredService<LinksDbContext>();
                    return !await db.Accounts.AnyAsync(x => x.Email.ToLower() == email.ToLower(), cancellationToken);
                })
                .WithMessage(ErrorMessages.AccountMessages.EmailAlreadyExists);
        }

        public static IRuleBuilderOptions<T, string> Password<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotNull()
                .WithMessage(ErrorMessages.AccountMessages.PasswordShouldNotBeNull)
                .NotEmpty()
                .WithMessage(ErrorMessages.AccountMessages.PasswordShouldNotBeEmpty)
                .MinimumLength(8)
                .WithMessage(ErrorMessages.AccountMessages.PasswordCannotBeLessThan8)
                .Matches(@"\W")
                .WithMessage(ErrorMessages.AccountMessages.PasswordMustContainSpecialSymbol)
                .Matches(@"\d")
                .WithMessage(ErrorMessages.AccountMessages.PasswordMustContainNumber);
        }

        public static IRuleBuilderOptions<T, int> LinkExists<T>(this IRuleBuilder<T, int> rule, IServiceScopeFactory serviceScopeFactory, string message = ErrorMessages.LinksErrors.LinkAndUserNotFound)
        {
            return rule
                .MustAsync(async (entityId, cancellationToken) =>
                {
                    using var scopeFactory = serviceScopeFactory.CreateScope();
                    var db = scopeFactory.ServiceProvider.GetRequiredService<LinksDbContext>();
                    return await db.Links.AnyAsync(x => x.Id == entityId, cancellationToken);
                })
                .WithMessage((cmd, entityId) => string.Format(message, entityId));
        }
    }
}
