﻿using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using LinksService.Models.Authentication;

namespace LinksService.Core.Infrastructure.Validators.Accounts
{
	public class SignInCommandValidator : BaseValidator<SignInCommand, SignInRequestModel>
	{
		private readonly IServiceScopeFactory _serviceScopeFactory;

		public SignInCommandValidator(IServiceScopeFactory serviceScopeFactory)
		{
			_serviceScopeFactory = serviceScopeFactory;
			ConfigureValidation();
		}

		protected override void ConfigureValidation()
		{
			base.ConfigureValidation();
			
			RuleFor(x => x.Model.Email)
                .EMail()
                .EmailExists(_serviceScopeFactory);

            RuleFor(x => x.Model.Password)
                 .Password();
        }
	}
}
