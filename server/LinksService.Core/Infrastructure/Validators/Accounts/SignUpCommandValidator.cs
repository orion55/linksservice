﻿using FluentValidation;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using LinksService.Models.Authentication;

namespace LinksService.Core.Infrastructure.Validators.Accounts
{
    public class SignUpCommandValidator : BaseValidator<SignUpCommand, SignUpRequestModel>
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SignUpCommandValidator(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
            ConfigureValidation();
        }

        protected override void ConfigureValidation()
        {
            base.ConfigureValidation();

            RuleFor(x => x.Model.Email)
                .EMail()
                .EmailNotExists(_serviceScopeFactory);

            RuleFor(x => x.Model.Password)
                .Password();

            RuleFor(x => x.Model)
                .Must(x => x.Password.Equals(x.PasswordConfirmation))
                .WithMessage(ErrorMessages.AccountMessages.PasswordAreNotTheSame);
        }
    }
}
