﻿using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.Models.Links;

namespace LinksService.Core.Infrastructure.Validators.Links
{
    public class CreateLinksCommandValidator : BaseValidator<CreateLinksCommand, CreateLinksRequestModel>
    {
        public CreateLinksCommandValidator()
        {            
            ConfigureValidation();
        }

        protected override void ConfigureValidation()
        {
            base.ConfigureValidation();
            
            RuleFor(x => x.Model.Url)
                .NotNull(ErrorMessages.LinksErrors.UrlShouldNotBeNull)
                .NotEmpty(ErrorMessages.LinksErrors.UrlShouldNotBeEmpty);
        }
    }
}
