﻿using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;

namespace LinksService.Core.Infrastructure.Validators.Links
{
    public class GetLinkCommandValidator : BaseValidator<GetLinkCommand, int>
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public GetLinkCommandValidator(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
            ConfigureValidation();
        }

        protected override void ConfigureValidation()
        {
            base.ConfigureValidation();

            RuleFor(x => x.Model)
                .LinkExists(_serviceScopeFactory);
        }
    }
}
