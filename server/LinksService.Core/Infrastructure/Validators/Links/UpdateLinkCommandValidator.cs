﻿using FluentValidation;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.Models.Links;

namespace LinksService.Core.Infrastructure.Validators.Links
{
    public class UpdateLinkCommandValidator : BaseValidator<UpdateLinkCommand, UpdateLinkModel>
    {
        public UpdateLinkCommandValidator()
        {
            ConfigureValidation();
        }

        protected override void ConfigureValidation()
        {
            base.ConfigureValidation();

            RuleFor(x => x.Model.Url)
                .NotNull()
                .WithMessage(ErrorMessages.LinksErrors.UrlShouldNotBeNull);                

            RuleFor(x => x.Model.IsActive)
                .NotNull()
                .WithMessage(ErrorMessages.LinksErrors.IsActiveShouldNotBeEmpty);                
                
        }
    }
}
