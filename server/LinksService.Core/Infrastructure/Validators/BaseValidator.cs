﻿using FluentValidation;
using LinksService.Core.Infrastructure.MediatR.Commands;
using LinksService.Core.Constants;

namespace LinksService.Core.Infrastructure.Validators
{
    public abstract class BaseValidator<TCommand, TModel> : AbstractValidator<TCommand>
        where TCommand : BaseModelCommand<TModel>
    {
        protected virtual void ConfigureValidation() 
        {
            RuleFor(x => x.Model)
                .NotNull()
                .WithMessage(ErrorMessages.ModelShouldNotBeNull);
        }
    }
}
