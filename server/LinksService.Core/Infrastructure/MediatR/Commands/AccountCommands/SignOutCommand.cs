﻿using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands
{
    public class SignOutCommand : IRequest<OneOf<Unit>>
    {

    }
}