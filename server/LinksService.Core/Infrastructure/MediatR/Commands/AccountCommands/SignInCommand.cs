﻿using System.Runtime.Serialization;
using MediatR;
using OneOf;
using LinksService.Models.Authentication;
using LinksService.Models.Common;

namespace LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands
{
    public class SignInCommand : BaseModelCommand<SignInRequestModel>, IRequest<OneOf<SignInResponseModel, BadRequestModel>>
    {
        public SignInCommand(SignInRequestModel model)
            : base(model)
        { }
    }
}