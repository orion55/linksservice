﻿using System.Runtime.Serialization;
using MediatR;
using OneOf;
using LinksService.Models.Authentication;
using LinksService.Models.Common;

namespace LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands
{
    public class SignUpCommand : BaseModelCommand<SignUpRequestModel>, IRequest<OneOf<SignUpResponseModel, BadRequestModel>>
    {
        public SignUpCommand(SignUpRequestModel model)
            : base(model)
        { }
    }
}
