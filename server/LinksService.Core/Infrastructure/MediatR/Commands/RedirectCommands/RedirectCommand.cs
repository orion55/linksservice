﻿using LinksService.Models.Common;
using LinksService.Models.Redirect;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.RedirectCommands
{
    public class RedirectCommand : BaseModelCommand<string>, IRequest<OneOf<RedirectModel, BadRequestModel>>
    {
        public RedirectCommand(string code): base(code)
        { }
    }
}
