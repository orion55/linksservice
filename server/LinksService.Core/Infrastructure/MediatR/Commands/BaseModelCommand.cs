﻿namespace LinksService.Core.Infrastructure.MediatR.Commands
{
    public abstract class BaseModelCommand<TModel>
    {
        public BaseModelCommand(TModel model)
        {
            Model = model;
        }

        public TModel Model { get; set; }
    }
}
