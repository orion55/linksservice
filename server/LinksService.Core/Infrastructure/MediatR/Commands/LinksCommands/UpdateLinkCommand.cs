﻿using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class UpdateLinkCommand : BaseModelCommand<UpdateLinkModel>, IRequest<OneOf<LinkModel, BadRequestModel>>
    {
        public int LinkId { get; set; }
        public UpdateLinkCommand(int linkId, UpdateLinkModel model) : base(model)
        {
            LinkId = linkId;
        }
    }
}
