﻿using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class GetLinkStatCommand : BaseModelCommand<int>, IRequest<OneOf<LinkStatModel, BadRequestModel>>
    {
        public GetLinkStatCommand(int linkId) : base(linkId)
        {

        }
    }
}
