﻿using LinksService.Models.Links;
using MediatR;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class GetStatLinksAllCommand : IRequest<StatLinksAllModel>
    {
    }
}
