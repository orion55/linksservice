﻿using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class GetLinkCommand : BaseModelCommand<int>, IRequest<OneOf<LinkModel, BadRequestModel>>
    {
        public GetLinkCommand(int linkId) : base(linkId)
        {

        }
    }
}
