﻿using LinksService.Models.Common;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class DeleteLinkCommand : BaseModelCommand<int>, IRequest<OneOf<DummyResponseModel, BadRequestModel>>
    {
        public DeleteLinkCommand(int linkId) : base(linkId) { }
    }
}
