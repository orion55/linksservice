﻿using LinksService.Models.Links;
using MediatR;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class GetLinksAllCommand : IRequest<ICollection<LinkModel>>
    {

    }
}
