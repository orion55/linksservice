﻿using System.Runtime.Serialization;
using MediatR;
using OneOf;
using LinksService.Models.Common;
using LinksService.Models.Links;

namespace LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands
{
    public class CreateLinksCommand : BaseModelCommand<CreateLinksRequestModel>, IRequest<OneOf<CreateLinksResponseModel, BadRequestModel>>
    {
        public CreateLinksCommand(CreateLinksRequestModel model) : base(model)
        {
        }
    }
}
