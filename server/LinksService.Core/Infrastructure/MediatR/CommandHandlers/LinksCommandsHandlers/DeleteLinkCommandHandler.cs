﻿using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class DeleteLinkCommandHandler : IRequestHandler<DeleteLinkCommand, OneOf<DummyResponseModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;        
        private readonly AuthHelper _authHelper;
        public DeleteLinkCommandHandler(LinksDbContext db, AuthHelper authHelper)
        {
            _db = db;            
            _authHelper = authHelper;
        }

        public async Task<OneOf<DummyResponseModel, BadRequestModel>> Handle(DeleteLinkCommand request, CancellationToken cancellationToken)
        {
            var userId = _authHelper.GetUserId();

            var link = await _db.Links.FirstOrDefaultAsync(l => l.Id == request.Model && l.AccountId == userId, cancellationToken: cancellationToken);

            if (link == null)
            {
                return new BadRequestModel(ErrorMessages.LinksErrors.LinkAndUserNotFound);
            }

            _db.Links.Remove(link);
            await _db.SaveChangesAsync(cancellationToken);

            return new DummyResponseModel();
        }
    }
}
