﻿using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Entities;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.UserSecrets;
using OneOf;
using shortid;
using shortid.Configuration;
using System.IdentityModel.Tokens.Jwt;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class CreateLinksCommandHandler : IRequestHandler<CreateLinksCommand, OneOf<CreateLinksResponseModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly AuthHelper _authHelper;
        public CreateLinksCommandHandler(LinksDbContext db, AuthHelper authHelper)
        {
            _db = db;
            _authHelper = authHelper;
        }
        async Task<OneOf<CreateLinksResponseModel, BadRequestModel>> IRequestHandler<CreateLinksCommand, OneOf<CreateLinksResponseModel, BadRequestModel>>.Handle(CreateLinksCommand request, CancellationToken cancellationToken)
        {
            var userId = _authHelper.GetUserId();

            if (userId != null) {
                var existingLink = _db.Links.AnyAsync(l => l.Url == request.Model.Url && l.AccountId == userId, cancellationToken: cancellationToken);
                if (existingLink.Result) return new BadRequestModel(ErrorMessages.LinksErrors.LinkAlreadyExists);
            }

            var options = new GenerationOptions(useSpecialCharacters: false, length: 9);
            string code = ShortId.Generate(options);

            var link = new Link { AccountId = userId, Url = request.Model.Url, Code = code, CreatedUtc = DateTime.UtcNow, IsActive = true };

            await _db.Links.AddAsync(link, cancellationToken);
            await _db.SaveChangesAsync();

            return new CreateLinksResponseModel
            {
                Code = code,
            };
        }
    }
}
