﻿using AutoMapper;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class UpdateLinkCommandHandler : IRequestHandler<UpdateLinkCommand, OneOf<LinkModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly IMapper _mapper;
        private readonly AuthHelper _authHelper;

        public UpdateLinkCommandHandler(LinksDbContext db, IMapper mapper, AuthHelper authHelper)
        {
            _db = db;
            _mapper = mapper;
            _authHelper = authHelper;
        }
        public async Task<OneOf<LinkModel, BadRequestModel>> Handle(UpdateLinkCommand request, CancellationToken cancellationToken)
        {
            var userId = _authHelper.GetUserId();

            var link = await _db.Links.FirstOrDefaultAsync(l => l.Id == request.LinkId && l.AccountId == userId, cancellationToken: cancellationToken);

            if (link == null)
            {
                return new BadRequestModel(ErrorMessages.LinksErrors.LinkAndUserNotFound);
            }

            link.Url = request.Model.Url;
            link.IsActive = request.Model.IsActive;            

            _db.Links.Update(link);
            await _db.SaveChangesAsync(cancellationToken);

            var result = _mapper.Map<LinkModel>(link);
            return result;
        }
    }
}
