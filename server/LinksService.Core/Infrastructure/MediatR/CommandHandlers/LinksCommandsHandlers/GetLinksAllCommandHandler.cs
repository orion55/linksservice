﻿using AutoMapper;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Infrastructure;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class GetLinksAllCommandHandler : IRequestHandler<GetLinksAllCommand, ICollection<LinkModel>>
    {
        private readonly LinksDbContext _db;
        private readonly AuthHelper _authHelper;
        private readonly IMapper _mapper;

        public GetLinksAllCommandHandler(LinksDbContext db, AuthHelper authHelper, IMapper mapper)
        {
            _db = db;
            _authHelper = authHelper;
            _mapper = mapper;
        }

        public async Task<ICollection<LinkModel>> Handle(GetLinksAllCommand request, CancellationToken cancellationToken)
        {
            var userId = _authHelper.GetUserId();
            
            var links = await _db.Links.Where(q => q.AccountId == userId)
                .OrderByDescending(p => p.CreatedUtc)
                .AsNoTracking()
                .ToArrayAsync();

            var result = _mapper.Map<LinkModel[]>(links);            
            return result;
        }
    }
}
