﻿using AutoMapper;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Entities;
using LinksService.DB.Infrastructure;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class GetStatLinksAllCommandHandler : IRequestHandler<GetStatLinksAllCommand, StatLinksAllModel>
    {
        private readonly LinksDbContext _db;
        private readonly AuthHelper _authHelper;
        public GetStatLinksAllCommandHandler(LinksDbContext db, AuthHelper authHelper)
        {
            _db = db;
            _authHelper = authHelper;
        }
        public async Task<StatLinksAllModel> Handle(GetStatLinksAllCommand request, CancellationToken cancellationToken)
        {
            var userId = _authHelper.GetUserId();

            var linkIDs = await _db.Links
                .Where(q => q.AccountId == userId)
                .OrderBy(p => p.Id)
                .AsNoTracking()
                .Select(x => x.Id)                
                .ToArrayAsync(cancellationToken);

            IQueryable<Stat> statsQuery = _db.Stats
                .Where(l => linkIDs.Contains(l.LinkId));

            var totalClicks = await statsQuery
                .AsNoTracking()
                .CountAsync(cancellationToken);
            
            var monthClicks = await statsQuery
                .Where(q => q.ClickUtc >= DateTime.UtcNow.AddMonths(-1))
                .AsNoTracking()
                .CountAsync(cancellationToken);

            var dayClicks = await statsQuery
               .Where(q => q.ClickUtc >= DateTime.Today)
               .AsNoTracking()
               .CountAsync(cancellationToken);

            var uniqIP = await statsQuery
                .AsNoTracking()
                .Select(x => x.IP)
                .Distinct()                
                .ToArrayAsync(cancellationToken);

            var uniqRefer = await statsQuery
                .AsNoTracking()
                .Select(x => x.Refer)
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Distinct()
                .ToArrayAsync(cancellationToken);
            
            var uniqUserAgent = await statsQuery
                .AsNoTracking()
                .Select(x => x.UserAgent)
                .Distinct()
                .ToArrayAsync(cancellationToken);
            
            return new StatLinksAllModel
            {
                CountLinks = linkIDs.Length,
                DayClicks = dayClicks,
                MonthClicks = monthClicks,
                TotalClicks = totalClicks,
                UniqIP = uniqIP,
                UniqRefer = uniqRefer,
                UniqUserAgent = uniqUserAgent,
            };
        }
    }
}
