﻿using AutoMapper;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Entities;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class GetLinkStatCommandHandler : IRequestHandler<GetLinkStatCommand, OneOf<LinkStatModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly IMapper _mapper;
        public GetLinkStatCommandHandler(LinksDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<OneOf<LinkStatModel, BadRequestModel>> Handle(GetLinkStatCommand request, CancellationToken cancellationToken)
        {
            IQueryable<Stat>  statsQuery =_db.Stats
                .Where(q => q.LinkId == request.Model);

            var stats = await statsQuery
                .OrderByDescending(p => p.ClickUtc)
                .AsNoTracking()
                .ToArrayAsync(cancellationToken);

            if (stats == null)
            {
                return new BadRequestModel(ErrorMessages.LinksErrors.LinkNotFound);
            }

            var records = _mapper.Map<StatModel[]>(stats);

            var monthClicks = await statsQuery
                .Where(q => q.ClickUtc >= DateTime.UtcNow.AddMonths(-1))                
                .AsNoTracking()
                .CountAsync(cancellationToken);

            var dayClicks = await statsQuery
               .Where(q => q.ClickUtc >= DateTime.Today)
               .AsNoTracking()
               .CountAsync(cancellationToken);

            return new LinkStatModel
            {
                Id = request.Model,
                TotalClicks = records.Length,
                MonthClicks = monthClicks,
                DayClicks = dayClicks,
                Records = records,
            };
        }
    }
}