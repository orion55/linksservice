﻿using AutoMapper;
using LinksService.Core.Infrastructure.MediatR.Commands.LinksCommands;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using LinksService.Models.Links;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.LinksCommandsHandlers
{
    public class GetLinkCommandHandler : IRequestHandler<GetLinkCommand, OneOf<LinkModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly IMapper _mapper;

        public GetLinkCommandHandler(LinksDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<OneOf<LinkModel, BadRequestModel>> Handle(GetLinkCommand request, CancellationToken cancellationToken)
        {
            var link = await _db.Links.FirstOrDefaultAsync(l => l.Id == request.Model, cancellationToken: cancellationToken);
            var result = _mapper.Map<LinkModel>(link);
            return result;
        }      
    }
}
