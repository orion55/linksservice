﻿using AutoMapper;
using LinksService.Core.Constants;
using LinksService.Core.Infrastructure.MediatR.Commands.RedirectCommands;
using LinksService.DB.Entities;
using LinksService.DB.Infrastructure;
using LinksService.Models.Common;
using LinksService.Models.Redirect;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.RedirectCommandsHandlers
{
    public class RedirectCommandHandler : IRequestHandler<RedirectCommand, OneOf<RedirectModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;        
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RedirectCommandHandler(LinksDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _db = db;            
            _httpContextAccessor = httpContextAccessor;
        }

        async Task<OneOf<RedirectModel, BadRequestModel>> IRequestHandler<RedirectCommand, OneOf<RedirectModel, BadRequestModel>>.Handle(RedirectCommand request, CancellationToken cancellationToken)
        {
            var link = await _db.Links.FirstOrDefaultAsync(l => l.Code == request.Model, cancellationToken: cancellationToken);
            if (link == null)
            {
                return new BadRequestModel(ErrorMessages.LinksErrors.LinkNotFound);
            }
            if (!link.IsActive)
            {
                return new BadRequestModel(ErrorMessages.LinksErrors.LinkNotActive);
            }

            var ip = _httpContextAccessor.HttpContext?.Connection.RemoteIpAddress?.ToString();
            var userAgent = _httpContextAccessor.HttpContext?.Request?.Headers["User-Agent"].ToString();
            var referer = _httpContextAccessor.HttpContext?.Request?.Headers["Referer"].ToString();

            var stat = new Stat { LinkId = link.Id, ClickUtc = DateTime.UtcNow, IP = ip, UserAgent = userAgent, Refer = referer };

            await _db.Stats.AddAsync(stat, cancellationToken);
            await _db.SaveChangesAsync();

            return new RedirectModel() { Url = link.Url };
        }
    }
}
