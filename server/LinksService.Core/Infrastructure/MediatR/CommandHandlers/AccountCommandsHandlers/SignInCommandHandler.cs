﻿using System.Security.Cryptography;
using System.Text;
using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using LinksService.DB.Infrastructure;
using LinksService.Models.Authentication;
using LinksService.Models.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;


namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.AccountCommandsHandlers
{
    public class SignInCommandHandler : IRequestHandler<SignInCommand, OneOf<SignInResponseModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly AuthHelper _authHelper;
        public SignInCommandHandler(LinksDbContext db, AuthHelper authHelper)
        {
            _db = db;
            _authHelper = authHelper;
        }

        public async Task<OneOf<SignInResponseModel, BadRequestModel>> Handle(SignInCommand request, CancellationToken cancellationToken)
        {

            var user = await _db.Accounts.FirstOrDefaultAsync(u => u.Email == request.Model.Email, cancellationToken: cancellationToken);
            if (user == null)
            {
                return new BadRequestModel("User not found!!!");
            }

            if (!VerifyPasswordHash(request.Model.Password, user.PasswordHash, user.PasswordSalt))
            {
                return new BadRequestModel("Password is incorrect.");
            }

            string token = _authHelper.CreateToken(user);

            return new SignInResponseModel
            {
                Token = token
            };
        }

        private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using var hmac = new HMACSHA512(passwordSalt);
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            return computedHash.SequenceEqual(passwordHash);
        }
    }
}
