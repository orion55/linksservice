﻿using LinksService.Core.Infrastructure.Helpers;
using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using LinksService.DB.Entities;
using LinksService.DB.Infrastructure;
using LinksService.Models.Authentication;
using LinksService.Models.Common;
using MediatR;
using Microsoft.Extensions.Configuration.UserSecrets;
using OneOf;
using System.Security.Cryptography;
using System.Text;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.AccountCommandsHandlers
{
    public class SignUpCommandHandler : IRequestHandler<SignUpCommand, OneOf<SignUpResponseModel, BadRequestModel>>
    {
        private readonly LinksDbContext _db;
        private readonly AuthHelper _authHelper;
        public SignUpCommandHandler(LinksDbContext db, AuthHelper authHelper)
        {
            _db = db;
            _authHelper = authHelper;
        }

        public async Task<OneOf<SignUpResponseModel, BadRequestModel>> Handle(SignUpCommand request, CancellationToken cancellationToken)
        {            
            CreatePasswordHash(request.Model.Password, out byte[] passwordHash, out byte[] passwordSalt);

            var account = new Account { Email = request.Model.Email, PasswordHash = passwordHash, PasswordSalt = passwordSalt, IsActive = true };
            await _db.Accounts.AddAsync(account, cancellationToken);
            await _db.SaveChangesAsync();

            string token = _authHelper.CreateToken(account);

            return new SignUpResponseModel
            {
                Token = token
            };
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using var hmac = new HMACSHA512();
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        }
    }
}