﻿using LinksService.Core.Infrastructure.MediatR.Commands.AccountCommands;
using MediatR;
using OneOf;

namespace LinksService.Core.Infrastructure.MediatR.CommandHandlers.AccountCommandsHandlers
{
    public class SignOutCommandHandler : IRequestHandler<SignOutCommand, OneOf<Unit>>
    {
        public SignOutCommandHandler()
        {
        }

        public async Task<OneOf<Unit>> Handle(SignOutCommand request, CancellationToken cancellationToken)
        {
            return Unit.Value;
        }
    }
}
