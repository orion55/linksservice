using FluentValidation;
using FluentValidation.Results;
using Reinforced.Typings.Ast.TypeNames;
using Reinforced.Typings.Fluent;
using System;
using System.Linq;
using System.Reflection;
using LinksService.Models.Authentication;
using LinksService.Core.Exceptions;

namespace DTOGenerator
{
	public static class ReinforcedTypingsConfiguration
	{
		public static void Configure(ConfigurationBuilder builder)
		{
			builder.Global(conf =>
			{
				conf.UseModules().CamelCaseForProperties();
				conf.AutoOptionalProperties(true);
			});

			builder.Substitute(typeof(Guid), new RtSimpleTypeName("string"));

			ConfigureFluentValidation(builder);
			ConfigureForAssembly(typeof(SignInRequestModel).Assembly, builder);
			ConfigureForAssembly(typeof(LinkServiceValidationException).Assembly, builder);
		}

		private static void ConfigureForAssembly(Assembly assembly, ConfigurationBuilder builder)
		{
			var types = assembly.GetExportedTypes();

			var interfaceTypes = types.Where(t =>
				t.GetProperties(BindingFlags.Public | BindingFlags.Instance).Length > 0 && t.IsClass && !t.IsAbstract ||
				t.IsInterface || t.IsValueType && !t.IsEnum
			);

			var enumTypes = types.Where(t => t.IsEnum);

			builder.ExportAsInterfaces(
				interfaceTypes,
				conf => conf.WithPublicProperties()
			);

			builder.ExportAsEnums(
				enumTypes
			);
		}
		
		private static void ConfigureFluentValidation(ConfigurationBuilder builder)
		{
			builder.ExportAsInterface<ValidationFailure>()
				.WithPublicProperties();

			builder.ExportAsEnum<Severity>();
		}
	}
}
