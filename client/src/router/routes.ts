export enum Routes {
	//Public
	Root = "/",
	Register = "/register/",
	Login = "/login/",
	Dashboard = "/dashboard/",
	Links = "/links/",
	LinksCreate = "/links/create/",
	LinksEdit = "/links/:id/edit/",
	LinksStat = "/links/:id/stat/",
}
