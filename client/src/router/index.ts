import { createRouter, createWebHistory } from "vue-router";
import { Routes } from "@/router/routes";
import HomePage from "@/pages/HomePage.vue";
import RegisterPage from "@/pages/RegisterPage.vue";
import LoginPage from "@/pages/LoginPage.vue";
import DashboardPage from "@/pages/DashboardPage.vue";
import { useAccountStore } from "@/store/account";
import LinksPage from "@/pages/LinksPage.vue";
import LinksCreatePage from "@/pages/LinksCreatePage.vue";
import LinksEditPage from "@/pages/LinksEditPage.vue";
import LinksStatPage from "@/pages/LinksStatPage.vue";

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: Routes.Root,
			name: "home",
			component: HomePage,
		},
		{
			path: Routes.Register,
			name: "register",
			component: RegisterPage,
		},
		{
			path: Routes.Login,
			name: "login",
			component: LoginPage,
		},
		{
			path: Routes.Dashboard,
			name: "dashboard",
			component: DashboardPage,
			meta: {
				requiresAuth: true,
			},
		},
		{
			path: Routes.Links,
			name: "links",
			component: LinksPage,
			meta: {
				requiresAuth: true,
			},
		},
		{
			path: Routes.LinksCreate,
			name: "links_create",
			component: LinksCreatePage,
			meta: {
				requiresAuth: true,
			},
		},
		{
			path: Routes.LinksEdit,
			name: "links_edit",
			component: LinksEditPage,
			meta: {
				requiresAuth: true,
			},
		},
		{
			path: Routes.LinksStat,
			name: "links_stat",
			component: LinksStatPage,
			meta: {
				requiresAuth: true,
			},
		},
	],
});
router.beforeEach((to, from, next) => {
	const storeAccount = useAccountStore();
	if (to.matched.some((record) => record.meta.requiresAuth)) {
		if (storeAccount.isAuthenticated) {
			next();
			return;
		}
		next({ name: "login" });
	} else {
		next();
	}
});
export default router;
