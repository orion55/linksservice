import { watch } from "vue";
import { ElLoading } from "element-plus";
import { useLinksStore } from "@/store/links";

let loading: any;
export default function useLoading(target: string) {
	const storeLinks = useLinksStore();
	const showLoading = () => (loading = ElLoading.service({ fullscreen: false, target }));
	const hideLoading = () => loading.close();

	watch(
		() => storeLinks.isLoading,
		(current) => {
			current ? showLoading() : hideLoading();
		}
	);
}
