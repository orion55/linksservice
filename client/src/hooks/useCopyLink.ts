import assignIn from "lodash/assignIn";
import isEmpty from "lodash/isEmpty";
import type { ILinkModel } from "@/models/dto/LinksService/Models/Links/ILinkModel";
import { createLink } from "@/helpers/links.helper";
import { copyTextToClipboard } from "@/helpers/clipboard.helper";
import { showSuccessMessage } from "@/helpers/error.handling.helper";
import { alertMessages } from "@/constants/messages";
import { useAccountStore } from "@/store/account";

export default function useCopyLink() {
	const storeAccount = useAccountStore();
	const handleCopy = (link: ILinkModel) => {
		const shareLink = createLink(String(link.code));
		handleCopyLink(shareLink);
	};
	const handleCopyLink = (link: string) => {
		if (!isEmpty(link))
			copyTextToClipboard(link).then(() => assignIn(storeAccount.alert, showSuccessMessage(alertMessages.copiedLink)));
	};

	return { handleCopy, handleCopyLink };
}
