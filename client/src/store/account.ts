import { defineStore } from "pinia";
import jwtDecode from "jwt-decode";
import isNull from "lodash/isNull";
import type { ISignUpRequestModel } from "@/models/dto/LinksService/Models/Authentication/ISignUpRequestModel";
import type { ISignInRequestModel } from "@/models/dto/LinksService/Models/Authentication/ISignInRequestModel";
import type { IAlert } from "@/helpers/error.handling.helper";
import { AccountApi } from "@/api/controllers/AccountApi";
import { clearAuthHeaders, setAuthHeaders } from "@/api/utils/api";

interface IUserInfo {
	id: number;
	email: string;
}

interface IDecode {
	aud: string;
	email: string;
	exp: number;
	iss: string;
	user_id: string;
}

interface IAccountState {
	accessToken: string | null;
	currentUserInfo: IUserInfo | null;
	isLoading: boolean;
	alert: IAlert;
}

export const useAccountStore = defineStore({
	id: "account",
	state: (): IAccountState => ({
		accessToken: null,
		currentUserInfo: null,
		isLoading: false,
		alert: { isShow: false, title: "", type: "success" },
	}),
	getters: {
		isAuthenticated(): boolean {
			return Boolean(this.accessToken);
		},
	},
	actions: {
		async signUp(data: ISignUpRequestModel) {
			this.isLoading = true;
			try {
				const response = await AccountApi.signUp(data);
				const token = response.data.data.token;
				if (token) {
					this.setAccessToken(token);
					this.initToken();
				}
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async signIn(data: ISignInRequestModel) {
			this.isLoading = true;
			try {
				const response = await AccountApi.signIn(data);
				const token = response.data.data.token;
				if (token) {
					this.setAccessToken(token);
					this.initToken();
				}
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		setAccessToken(token: string) {
			this.accessToken = token;
		},
		getCurrentUser(token: string) {
			const decoded: IDecode = jwtDecode(token);
			this.currentUserInfo = {
				id: Number(decoded.user_id),
				email: decoded.email,
			};
		},
		initToken() {
			if (!isNull(this.accessToken)) {
				setAuthHeaders(this.accessToken);
				this.getCurrentUser(this.accessToken);
			}
		},
		async signOut() {
			this.accessToken = null;
			await AccountApi.signOut({});
			clearAuthHeaders();
		},
	},
	persist: {
		enabled: true,
		strategies: [{ storage: localStorage, paths: ["accessToken"] }],
	},
});
