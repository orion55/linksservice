import { defineStore } from "pinia";
import type { ICreateLinksRequestModel } from "@/models/dto/LinksService/Models/Links/ICreateLinksRequestModel";
import type { IUpdateLinkModel } from "@/models/dto/LinksService/Models/Links/IUpdateLinkModel";
import { LinksApi } from "@/api/controllers/LinksApi";

export const useLinksStore = defineStore({
	id: "links",
	state: () => ({ isLoading: false }),
	getters: {},
	actions: {
		async createLinks(data: ICreateLinksRequestModel) {
			this.isLoading = true;
			try {
				const response = await LinksApi.create(data);
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async getLink(linkId: number) {
			this.isLoading = true;
			try {
				const response = await LinksApi.get(linkId);
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async getLinkAll() {
			this.isLoading = true;
			try {
				const response = await LinksApi.getAll();
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async updateLink(linkId: number, data: IUpdateLinkModel) {
			const response = await LinksApi.update(linkId, data);
			return response.data.data;
		},
		async updateLinkLoading(linkId: number, data: IUpdateLinkModel) {
			this.isLoading = true;
			try {
				return await this.updateLink(linkId, data);
			} finally {
				this.isLoading = false;
			}
		},
		async deleteLink(linkId: number) {
			this.isLoading = true;
			try {
				const response = await LinksApi.delete(linkId);
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async stat(linkId: number) {
			this.isLoading = true;
			try {
				const response = await LinksApi.stat(linkId);
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
		async statAll() {
			this.isLoading = true;
			try {
				const response = await LinksApi.statAll();
				return response.data.data;
			} finally {
				this.isLoading = false;
			}
		},
	},
});
