import { shallowRef } from "vue";
import TelegramIcon from "@/assets/icons/telegram.svg";
import WhatappIcon from "@/assets/icons/whatsapp.svg";
import VkIcon from "@/assets/icons/vk.svg";
import FacebookIcon from "@/assets/icons/facebook.svg";
import TwitterIcon from "@/assets/icons/twitter.svg";
import SkypeIcon from "@/assets/icons/skype.svg";
import ViberIcon from "@/assets/icons/viber.svg";
import OdnoklassnikiIcon from "@/assets/icons/odnoklassniki.svg";

export const color = { dark: "#000000ff", light: "#ffffffff" };

export interface ILink {
	id: number;
	component: any;
	href: string;
}

export const getLinks = (shareLink: string): ILink[] => {
	return [
		{
			id: 0,
			component: shallowRef(TelegramIcon),
			href: `https://t.me/share/url?url=${shareLink}&text=Short%20link%20service`,
		},
		{ id: 1, component: shallowRef(WhatappIcon), href: `https://api.whatsapp.com/send?text=${shareLink}` },
		{
			id: 2,
			component: shallowRef(VkIcon),
			href: `https://vk.com/share.php?url=${shareLink}&title=Short%20link%20service`,
		},
		{
			id: 3,
			component: shallowRef(FacebookIcon),
			href: `https://www.facebook.com/sharer.php?src=sp&u=${shareLink}&title=Short%20link%20service`,
		},
		{ id: 4, component: shallowRef(TwitterIcon), href: `https://twitter.com/intent/tweet?text=${shareLink}` },
		{
			id: 5,
			component: shallowRef(SkypeIcon),
			href: `https://web.skype.com/share?url=${shareLink}`,
		},
		{ id: 6, component: shallowRef(ViberIcon), href: `viber://forward?text=${shareLink}` },
		{
			id: 7,
			component: shallowRef(OdnoklassnikiIcon),
			href: `https://connect.ok.ru/offer?url=${shareLink}&title=Short%20link%20service`,
		},
	];
};
