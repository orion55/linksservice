import Api from "../utils/api";
import type { ISignInRequestModel } from "@/models/dto/LinksService/Models/Authentication/ISignInRequestModel";
import type { ISignInResponseModel } from "@/models/dto/LinksService/Models/Authentication/ISignInResponseModel";
import type { ISignUpRequestModel } from "@/models/dto/LinksService/Models/Authentication/ISignUpRequestModel";
import type { ISignUpResponseModel } from "@/models/dto/LinksService/Models/Authentication/ISignUpResponseModel";

class IDummyRequestModel {}

class IDummyResponseModel {}

export class AccountApi {
	static signIn(data: ISignInRequestModel) {
		return Api.post<ISignInResponseModel>("Account/SignIn", data);
	}

	static signUp(data: ISignUpRequestModel) {
		return Api.post<ISignUpResponseModel>("Account/SignUp", data);
	}

	static signOut(data: IDummyRequestModel) {
		return Api.post<IDummyResponseModel>("Account/SignOut", data);
	}
}
