import type { ICreateLinksRequestModel } from "@/models/dto/LinksService/Models/Links/ICreateLinksRequestModel";
import type { ICreateLinksResponseModel } from "@/models/dto/LinksService/Models/Links/ICreateLinksResponseModel";
import type { ILinkModel } from "@/models/dto/LinksService/Models/Links/ILinkModel";
import type { ILinkStatModel } from "@/models/dto/LinksService/Models/Links/ILinkStatModel";
import type { IStatLinksAllModel } from "@/models/dto/LinksService/Models/Links/IStatLinksAllModel";
import type { IUpdateLinkModel } from "@/models/dto/LinksService/Models/Links/IUpdateLinkModel";
import Api from "@/api/utils/api";

export class LinksApi {
	static create(data: ICreateLinksRequestModel) {
		return Api.post<ICreateLinksResponseModel>("Links/Create", data);
	}

	static get(linkId: number) {
		return Api.get<ILinkModel>(`Links/${linkId}`);
	}

	static getAll() {
		return Api.get<ILinkModel[]>("Links");
	}

	static update(linkId: number, data: IUpdateLinkModel) {
		return Api.put<ILinkModel>(`Links/${linkId}`, data);
	}

	static delete(linkId: number) {
		return Api.delete<ILinkModel>(`Links/${linkId}`);
	}

	static stat(linkId: number) {
		return Api.get<ILinkStatModel>(`Links/${linkId}/stat`);
	}

	static statAll() {
		return Api.get<IStatLinksAllModel>("Links/stat");
	}
}
