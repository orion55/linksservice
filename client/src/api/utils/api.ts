import isNil from "lodash/isNil";
import axios from "axios";
import type { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import type { IResponseModel } from "@/models/dto/LinksService/Models/Common/IResponseModel";
import type { IBadRequestModel } from "@/models/dto/LinksService/Models/Common/IBadRequestModel";
import type { IBadRequestValidationFailedModel } from "@/models/dto/LinksService/Models/Common/IBadRequestValidationFailedModel";
import type { IValidationFailure } from "@/models/dto/FluentValidation/Results/IValidationFailure";
import type { IRequestModel } from "@/models/dto/LinksService/Models/Common/IRequestModel";

export const ApiBase: AxiosInstance = axios.create();

export interface ApiResponse<T> {
	data: T;
	error?: AxiosError;
}

export const initAxios = (errorHandler: (error: AxiosError) => void) => {
	const apiUrl = import.meta.env.VITE_API_URL;

	ApiBase.defaults.baseURL = `${apiUrl}/api/`;
	ApiBase.defaults.responseType = "json";

	ApiBase.interceptors.response.use(
		(response: AxiosResponse) => response,
		(error: AxiosError) => {
			errorHandler && errorHandler(error);
			return Promise.reject(error);
		}
	);
};

const commonHeaders: Record<string, string> = ApiBase.defaults.headers?.common as any;

export const clearAuthHeaders = () => {
	delete commonHeaders["Authorization"];
};

export const setAuthHeaders = (token: string) => {
	clearAuthHeaders();
	if (token) {
		commonHeaders["Authorization"] = `Bearer ${token}`;
	}
};

export function wrapRequestModel<T>(data?: T) {
	return {
		params: data,
	} as IRequestModel<T>;
}

export function isApiError<T>(payload: any): payload is AxiosError<IResponseModel<T>> {
	return axios.isAxiosError(payload);
}

export function isBadRequest(payload: any): payload is AxiosError<IResponseModel<IBadRequestModel>> {
	return isApiError<IBadRequestModel>(payload);
}

export function isValidationBadRequest(
	payload: any
): payload is AxiosError<IResponseModel<IBadRequestValidationFailedModel>> {
	return isApiError<IBadRequestValidationFailedModel>(payload);
}

export function getValidationErrorMessage(payload: any): string {
	if (!isNil(payload.data?.data?.validationFailures)) {
		return payload.data?.data?.validationFailures.find((err: IValidationFailure) => err.errorMessage)?.errorMessage;
	}
	return payload.data?.data?.errorMessage;
}

export default class Api {
	static get<T = any>(url: string, config?: AxiosRequestConfig<IResponseModel<T>>) {
		return ApiBase.get<IResponseModel<T>>(url, config);
	}

	static post<R = any, T = any>(url: string, data?: T, config?: AxiosRequestConfig<IRequestModel<T>>) {
		return ApiBase.post<IRequestModel<T>, AxiosResponse<IResponseModel<R>>>(url, wrapRequestModel(data), config);
	}

	static postUnWrap<R = any, T = any>(url: string, data?: T, config?: AxiosRequestConfig<T>) {
		return ApiBase.post<T, AxiosResponse<IResponseModel<R>>>(url, data, config);
	}

	static put<R = any, T = any>(url: string, data?: T, config?: AxiosRequestConfig<IRequestModel<T>>) {
		return ApiBase.put<IRequestModel<T>, AxiosResponse<IResponseModel<R>>>(url, wrapRequestModel(data), config);
	}

	static delete<R = any, T = any>(url: string, data?: T) {
		return ApiBase.delete<IRequestModel<T>, AxiosResponse<IResponseModel<R>>>(url, wrapRequestModel(data));
	}
}
