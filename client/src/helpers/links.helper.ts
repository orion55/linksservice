export const createLink = (code: string) => {
	const apiUrl = import.meta.env.VITE_API_URL;
	return `${apiUrl}/t/${code}`;
};
