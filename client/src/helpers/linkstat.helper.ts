export interface ILinkStat {
	countLinks: number;
	totalClicks: number;
	monthClicks: number;
	dayClicks: number;
}
