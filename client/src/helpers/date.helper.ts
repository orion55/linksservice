import { DateTime } from "luxon";

export const getDate = (date: string) => DateTime.fromISO(date).setLocale("ru").toLocaleString(DateTime.DATETIME_MED);
