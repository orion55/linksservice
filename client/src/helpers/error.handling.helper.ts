import type { AxiosError } from "axios";
import type { IBadRequestModel } from "@/models/dto/LinksService/Models/Common/IBadRequestModel";
import type { IBadRequestValidationFailedModel } from "@/models/dto/LinksService/Models/Common/IBadRequestValidationFailedModel";
import type { IResponseModel } from "@/models/dto/LinksService/Models/Common/IResponseModel";
import { isApiError } from "@/api/utils/api";

export interface IAlert {
	isShow: boolean;
	title: string;
	type: "success" | "warning" | "info" | "error";
}
export interface IErrorInfo {
	isError: boolean;
	errorMessage: string;
}

export const emptyErrorInfo = (): IErrorInfo => ({
	isError: false,
	errorMessage: "",
});

export const makeErrorInfo = (errorMessage: string): IErrorInfo => ({
	isError: true,
	errorMessage,
});

export const isApiBadRequestModel = (obj: any): obj is IBadRequestValidationFailedModel => {
	return "errorMessage" in obj && "title" in obj && "errorMessages" in obj;
};

export const isApiValidationFailedModel = (obj: any): obj is IBadRequestValidationFailedModel => {
	return isApiBadRequestModel(obj) && "validationFailures" in obj;
};

export type LinksError =
	| string
	| AxiosError<IResponseModel<IBadRequestModel>>
	| AxiosError<IResponseModel<IBadRequestValidationFailedModel>>
	| unknown;

export const showErrorMessage = (err: LinksError): IAlert => {
	let errorMessage: string | null | undefined = null;
	if (typeof err === "string") {
		errorMessage = err;
	} else if (isApiError<IBadRequestModel | IBadRequestValidationFailedModel>(err)) {
		const data = err.response?.data.data;
		if (data) {
			if (isApiValidationFailedModel(data)) {
				errorMessage = data?.validationFailures?.map((e) => e.errorMessage)?.join(", ");
			}
			if (!errorMessage) {
				errorMessage = data?.errorMessages?.join(", ");
			}
		}
		if (!errorMessage) {
			errorMessage = err.response?.data.data?.errorMessage;
		}
		if (!errorMessage) {
			errorMessage = err.message;
		}
	}

	const notHandledCorrectly = !errorMessage;

	if (!errorMessage) {
		errorMessage = String(err);
	}

	if (notHandledCorrectly) throw err;
	else return { isShow: true, title: errorMessage, type: "error" } as IAlert;
};

export const showSuccessMessage = (message: string): IAlert => {
	return { isShow: true, title: message, type: "success" } as IAlert;
};
