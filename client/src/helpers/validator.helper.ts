import * as Yup from "yup";
import YupPassword from "yup-password";
import { string } from "yup";
import { validatorMessages } from "@/constants/messages";

YupPassword(Yup);
export const urlValidator = string().required(validatorMessages.required).url(validatorMessages.validUrl);
export const emailValidator = Yup.string().required(validatorMessages.required).email(validatorMessages.invalidEmail);

export const passwordValidator = Yup.string()
	.password()
	.min(8, validatorMessages.minPassword)
	.max(256, validatorMessages.maxPassword)
	.minLowercase(1, validatorMessages.minLowercase)
	.minUppercase(1, validatorMessages.minUppercase)
	.minNumbers(1, validatorMessages.minNumbers)
	.minSymbols(1, validatorMessages.minSymbols)
	.required(validatorMessages.required);

export const passwordConfirmationValidator = passwordValidator.oneOf(
	[Yup.ref("password"), null],
	validatorMessages.matchPassword
);
