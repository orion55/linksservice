import { createApp } from "vue";
import { createPinia } from "pinia";
import piniaPersist from "pinia-plugin-persist";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import App from "./App.vue";
import router from "./router";
import "../node_modules/modern-normalize/modern-normalize.css";
import "./styles/styles.scss";
import type { AxiosError } from "axios";
import { initAxios } from "@/api/utils/api";

const pinia = createPinia();
pinia.use(piniaPersist);
const app = createApp(App);

// @ts-ignore
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
	app.component(key, component);
}
initAxios((error: AxiosError) => {
	if (error && error.response?.status === 401 && !error.config?.url?.includes("account/signout")) {
		console.warn("Error. Unauthorized. Probably token was expired.");
	}
});
app.use(pinia);
app.use(router);
app.mount("#app");
